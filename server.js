// server.js

// set up ======================================================================
// get all the tools we need
var express  = require('express');
var app      = express();
var port     = process.env.PORT || 8080;
//var flash    = require('connect-flash');

var morgan       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');
var compression = require('compression');


// configuration ===============================================================


// set up our express application
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.json()); // get information from html forms
app.use(bodyParser.urlencoded({ extended: true }));

app.set('view engine', 'ejs'); // set up ejs for templating

// required for passport
//app.use(flash()); // use connect-flash for flash messages stored in session

// required for passport
//app.use(flash()); // use connect-flash for flash messages stored in session

 // config controllers ======================================================================
require('./app/controllerPrueba')(app, null); // load our routes and pass in our app and fully configured passport

// local resources
app.use(express.static(__dirname + '/public'));
var compression = require('compression');


// launch ======================================================================
app.listen(port);
console.log('Sistema de precedencia listo por el puerto' + port);
